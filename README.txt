DESCRIPTION
--------------------------
This module will capture and aggregate user session data in order to provide the following:

 - Elapsed Time: total time a user has spent on the site
 - Session Count: total number of unique sessions for a user
 - Date: A time stamp since the data started being collected


INSTALLATION
--------------------------
Install as usual, see http://drupal.org/node/70151 for further information.

SETUP
--------------------------
Enable this module as well as the Automated Logout and Session Limit modules.
Configure the Automated Logout and Session Limit modules as you see fit, its
strongly recommended to limit the sessions to 1. The Session Aggregator module
will start recording data the next time users log in.

DEPENDENCIES
--------------------------
 - Automated Logout (6.x-4.x)
 - Session Limit

VIEWS
--------------------------
This module depends on Views for displaying session aggregation data. Create
any blocks or pages you may need with Views.  The following fields are available.

Fields:
  - Unique Sessions
    The number of unique sessions or logins the user has

  - Elapsed Time
    The total time a user has been active on the site

  - Since
    The date since Session Aggregator started logging data for the user

Each of the above fields has their respective sort and filter.

How it works
--------------------------
When a user logs in, the session count field is incremented for a user and 
their session id and creation time are saved for later use. When the user is 
logged out, the session's time span is added to the user's elapsed time.

When a session collision is detected by the Session Limit module the time span 
of the session being closed is added to the user's elapsed time if the time 
span of the closed session is less than the time limit specified by the 
Automated Logout module. But if the time span of the closed session is greater 
than time limit, meaning a hung session has been detected, the time limit 
specified by the Automated Logout module is added to the user's elapsed time.

Its important to remember that the elapsed time field is only updated during
a successfull logout or when a session collision is detected. So the elapsed 
time is not up to date for currently logged in users.