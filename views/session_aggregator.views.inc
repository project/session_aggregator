<?php

/**
 * @file Contains the Views api functions 
 */

/**
 * Implementation of hook_views_data().
 */
function session_aggregator_views_data() { 
  $data['session_aggregator']['table']['group']  = t('Session Aggregator');
  $data['session_aggregator']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['session_aggregator']['unique_sessions'] = array(
    'title' => t('Unique Sessions'),
    'help' => t('The number of unique sessions or logins the user has.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
     'handler' => 'views_handler_sort',
   ),
   'filter' => array(
     'handler' => 'views_handler_filter_numeric',
   ),
  );
  
  $data['session_aggregator']['elapsed_time'] = array(
   'title' => t('Elapsed Time'),
   'help' => t("The total time a user has been active on the site"),
   'field' => array(
     'handler' => 'views_handler_field_time_interval',
     'click sortable' => TRUE,
   ),
   'sort' => array(
     'handler' => 'views_handler_sort',
   ),
   'filter' => array(
     'handler' => 'views_handler_filter_numeric',
   ),
  );
  
  $data['session_aggregator']['since'] = array(
   'title' => t('Since'),
   'help' => t("The date since Session Aggregator started logging data for the user."),
   'field' => array(
     'handler' => 'views_handler_field_date',
     'click sortable' => TRUE,
   ),
   'sort' => array(
     'handler' => 'views_handler_sort_date',
   ),
   'filter' => array(
     'handler' => 'views_handler_filter_date',
   ),
  );

  return $data;
}